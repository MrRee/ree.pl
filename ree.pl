#!/usr/bin/perl
# ree.pl by MrRee
# MrRee <MrRee@6697.co.uk>
# For help with this script please join #ree @ irc.6697.co.uk
use HexChat qw( :all );
use Config;
use LWP::Simple qw/get/;
use JSON;
use Text::Glob qw( match_glob glob_to_regex );

# define defaults:
my $ree_version = 0.06;
my $ree_text_dir = HexChat::get_info('configdir')."/addons/ree";

if(!HexChat::plugin_pref_get('ree.debug')) { HexChat::plugin_pref_set('ree.debug', 'off'); }

# PM Firewall
if(!HexChat::plugin_pref_get('ree.pmfw.enabled')) { HexChat::plugin_pref_set('ree.pmfw.enabled', 'off'); }
# Handlers
if(!HexChat::plugin_pref_get('ree.handlers.wallops')) { HexChat::plugin_pref_set('ree.handlers.wallops', 'on'); }
if(!HexChat::plugin_pref_get('ree.handlers.ctcp_random_version')) { HexChat::plugin_pref_set('ree.handlers.ctcp_random_version', 'on'); }
if(!HexChat::plugin_pref_get('ree.autoaway.timer')) { HexChat::plugin_pref_set('ree.autoaway.timer',1200000); }

# Updater
if(!HexChat::plugin_pref_get('ree.update.startup.check')) { HexChat::plugin_pref_set('ree.update.startup.check', 'on'); }
if(!HexChat::plugin_pref_get('ree.update.startup.run')) { HexChat::plugin_pref_set('ree.update.startup.run', 'off'); }


# PM Firewall defaults & hardcoded stuff
my %ree_pmfw_mask_list = {};
$ree_pmfw_mask_list{"\%*\!\*@\*"} = 1;


HexChat::register( "ree.pl", $ree_version, "For help join #ree @ irc.6697.co.uk", "" );

sub ree_init {
    ree_msg("Started ree.pl v".$ree_version." by MrRee! If you need any help please join #ree on irc.6697.co.uk");
    # OS Check
    if(!$Config{'osname'})
	{
		ree_msg("Failed to detect operating system, please add the following error line to an issue at: https://gitlab.com/MrRee/ree.pl/issues");
		ree_msg("\00304Error:\003\tFailed to detect operating system at all;");
	} elsif($Config{'osname'} =~ /linux/i)
	{
        ree_msg("    .--.");
        ree_msg("   |o_o |");
        ree_msg("   |:_/ |");
        ree_msg("  //   \\ \\");
        ree_msg(" (|     | )");
        ree_msg("/'\\_   _/`\\");
        ree_msg("\\___)=(___/");
		ree_msg("Detected OS as Linux, yay! That means that your operating system is supported!");
		HexChat::hook_command('ree-np', "ree_np_rhythmbox"); # Hook now playing to rhythmbox
	} elsif($Config{'osname'} =~ /mswin32/i)
	{
		# detected operating system as windows.
        # how? How the hell did you actually get this working?
		ree_msg("\00304WARNING!\003\tDetected OS as Windows: This may affect the way some features of ree.pl work.");
        ree_msg("We do not officially support running this script on Windows, and probably no clue how you got it working!");
        ree_msg("Since you have this actually working on Windows you should write a tutorial in the issues for this script so that we can reproduce it: https://gitlab.com/MrRee/ree.pl/issues");
	} else {
		ree_msg("Failed to detect operating system, please add the following error line to an issue at: https://gitlab.com/MrRee/ree.pl/issues");
		ree_msg("\00304Error:\003\tFailed to detect operating system from: ".$Config{'osname'}.";");
	}
    if(HexChat::plugin_pref_get('ree.update.startup.check') =~ /^on$/i)
    {
        ree_update_check(); # check for updates.
    }
    if(HexChat::plugin_pref_get('ree.update.startup.run') =~ /^on$/i)
    {
        ree_update_run();
        ree_msg("Auto installation of update complated, attempting to reload the script...");
        HexChat::command("RELOAD ree.pl");
    }
    ree_pmfw_load_rules(); # load firewall rules

    # Hook non OS specific commands:
    HexChat::hook_command("ree-update", "ree_update_check",{help_text => "Checks if there is an update for ree.pl"});
    HexChat::hook_command("ree-install-update", "ree_update_run",{help_text => "Checks if there is an update for ree.pl and installs it."});
    HexChat::hook_command("ree-help", "ree_help_menu", {help_text => "Brings up a help menu"});
    HexChat::hook_command("ree-set", "ree_set", {help_text => "View & Set ree.pl settings"});
    HexChat::hook_command("ree-slap", "ree_slap"), {help_text => "\002Usage:\002 /ree-slap <nickname>"};
    HexChat::hook_command("ree-give", "ree_give"), {help_text => "\002Usage:\002 /ree-give <nickname>"};
    HexChat::hook_command("ree-pmfw", "ree_pm_firewall_cmd", {help_text => "Run without arguments for help"});
    HexChat::hook_command("ree-printfromfile", "ree_print_from_file", {help_text => "A development command"});
    HexChat::hook_command('ree-rainbow', 'ree_rainbow', {help_text => "\002Usage:\002 /ree-rainbow <text>"});
    HexChat::hook_command('ree-ascii', 'ree_ascii', {help_text => "\002Usage:\002 /ree-ascii <path>"});
    HexChat::hook_command('ree-setidletimer', "ree_set_idle_time");
    HexChat::hook_command('ree-ipinfo', "ree_ip_info");
    HexChat::hook_command('ree-cah', 'ree_cah');

    my @servers = ();
    my @timers = ();
    hook_print("Your Message", "ree_timer_check");
    hook_print("Your Action", "ree_timer_check");
    hook_print("Connected", "ree_add_new_server");
    ree_populate_servers();


    # Server Hooks
    HexChat::hook_server("WALLOPS", "ree_handler_wallops");
    HexChat::hook_server("PRIVMSG", "ree_handler_privmsg");
    HexChat::hook_server("PRIVMSG", "ree_handler_ctcp");
    HexChat::hook_server("PRIVMSG", "ree_handler_pmfw");
    HexChat::hook_server("JOIN", "ree_handler_onJoin");
    HexChat::hook_server("PART", "ree_handler_onPart");
}

sub ree_msg {
    my ($msg) = @_;
    HexChat::command("ECHO \002\00318[REE]\003\002\t".$msg);
}

sub ree_msg_window {
    my ($window, $msg) = @_;
    HexChat::command("RECV :\%".$window."!ree.pl@ree.pl PRIVMSG ".HexChat::get_info('nick')." :".$msg)
}

sub ree_update_run {
    my $ree_update_content = get 'https://gitlab.com/api/v4/projects/13179260/releases';
    if(!$ree_update_content) 
    { 
        ree_msg("Warning! Could not get latest version information for ree.pl.");
    } else {
        if ( $ree_update_content =~ /\"tag_name\"\:\"([0-9\.]+).*?\"/i )
        {
            my $ree_latest_version = $1;
            if($ree_latest_version > $ree_version)
            {
                ree_msg("Attempting to download and update to ree.pl v".$ree_latest_version);
                my $ree_update_script = get 'https://gitlab.com/MrRee/ree.pl/raw/'.$ree_latest_version.'/ree.pl';
                open(my $fh, '>', HexChat::get_info('configdir')."/addons/ree.pl") or return 0;
                print $fh $ree_update_script;
                close $fh;
                ree_msg("Installation of latest ree.pl was a success! Now make sure that you have updated any new dependencies and run /reload ree.pl")
            }
        }
    }
    HexChat::EAT_ALL;   
}
sub ree_update_check {
    my $ree_update_content = get 'https://gitlab.com/api/v4/projects/13179260/releases';
    if(!$ree_update_content) 
    { 
        ree_msg("Warning! Could not get latest version information for ree.pl.");
    } else {
        if ( $ree_update_content =~ /\"tag_name\"\:\"([0-9\.]+).*?\"/i )
        {
            my $ree_latest_version = $1;
            if($ree_latest_version > $ree_version)
            {
                ree_msg("A new version of ree.pl is available at https://www.6697.co.uk/code/hexchat/ree.html");
                ree_msg("You can update to the latest version by typing /ree-install-update");
            }
        }
    }
    HexChat::EAT_ALL;
}

sub ree_help_menu
{
    HexChat::print(["\002ree.pl v".$ree_version." by MrRee <MrRee@6697.co.uk>\002\n",
        "If you need help beyond the scope of this help text then please join \00318#ree\003 on \00318irc.6697.co.uk\003\n\n",
        "ree-give\t\t- Give someone a random object.\n",
        "ree-help\t\t- Display this help text.\n",
        "ree-ipinfo\t\t- Get information about the given IP address.\n",
        "ree-np\t\t- Display nowplaying from Rhythmbox media player.\n",
        "ree-pmfw\t\t- PM Firewall system. (to use /ree-set pmfw.enabled on).\n",
        "ree-rainbow\t- Makes your text colourful!\n",
        "ree-slap\t\t- Slap someone with a random object.\n",
        "ree-update\t- Check for updates for ree.pl\n"]);
    HexChat::EAT_ALL;
}

sub ree_set
{
    if(!$_[0][1])
    {
        HexChat::print([
            "\002ree.pl Settings\002\n",
            "\002PM Firewall\002\n",
            "pmfw.enabled\t".HexChat::plugin_pref_get('ree.pmfw.enabled')."\n",
            "\002Handlers:\002\n",
            "handlers.ctcp_random_version\t".HexChat::plugin_pref_get('ree.handlers.ctcp_random_version')."\n",
            "handlers.wallops:\t".HexChat::plugin_pref_get('ree.handlers.wallops')
        ]);
    } elsif(!$_[0][2]) {
        ree_msg($_[0][1].": ".HexChat::plugin_pref_get('ree.'.$_[0][1]));
    } elsif(!HexChat::plugin_pref_get('ree.'.$_[0][1]))
    {
        ree_msg("Invalid setting: " + $_[0][1]);
    } else {
        my $setting = $_[0][2];
        HexChat::plugin_pref_set('ree.'.$_[0][1], $setting);
        ree_msg($_[0][1].": ".HexChat::plugin_pref_get('ree.'.$_[0][1]));
    }
    HexChat::EAT_ALL;
}

sub ree_ip_info
{
    my $ip = $_[0][1];
    my $data = JSON::decode_json(get "https://ipinfo.io/".$ip);
    ree_msg($data->{'ip'}." (".$data->{'hostname'}.") ".$data->{'city'}.", ".$data->{'region'}." - ".$data->{'country'}.", ".$data->{'postal'}." [".$data->{'org'}."]");
    HexChat::EAT_ALL;
}

sub ree_rainbow {
    # Mostly borrowed from another script, rainbowify, by William Woodruff.
	my $text = $_[1][1];
	if (defined $text) {
		$text =~ s/(.)/"\cC" . (int(rand(14))+2) . "$1"/eg;
		HexChat::command("say ".$text);
	}
	return HexChat::EAT_ALL;
}

sub ree_np_rhythmbox
{
    my $output = `rhythmbox-client --print-playing-format '\%tt \- \%ta'`;
	chomp($output); # chomp output to prevent extra data being sent to the server.
	HexChat::command("ME NP: ".$output);
	return HexChat::EAT_HEXCHAT;
}

sub ree_handler_wallops
{
    if(HexChat::plugin_pref_get('ree/handlers/wallops') =~ /^on$/i)
    {
        #:MrRee!MrRee@6697.co.uk WALLOPS :this is a test
        if($_[1][0] =~ /^\:(.*?)\!(.*?)\@(.*?) WALLOPS :(.*?)$/i)
        {
            ree_msg_window("WALLOPS", "<".$1."> ".$4);
            HexChat::EAT_HEXCHAT;
        }
    }
}

sub ree_cah
{
     unless (-e $ree_text_dir."/questions.txt") {
         ree_get_missing_file("questions.txt");
     }
     unless (-e $ree_text_dir."/answers.txt") {
         ree_get_missing_file("answers.txt");
     }
    my $line = ree_random_line_from_file($ree_text_dir."/questions.txt");
    if(!$line)
    {
        ree_msg("\00304Error!:\003\tCould not open questions file!");
        return HexChat::EAT_ALL;
    }
    my $count = $line =~ tr/\_//;
    $repc = 0;
    if($count == 0)
    {
        $line = $line." ".ree_random_line_from_file($ree_text_dir."/answers.txt");
    } else {
        while($count >= $repc)
        {
            my $replace = ree_random_line_from_file($ree_text_dir."/answers.txt");
            if(!$replace)
            {
                ree_msg("\00304Error!:\003\tCould not open answers file!");
                return HexChat::EAT_ALL;
            }
            $line =~ s/\_/$replace/;
            $repc++;
        }
    }
    HexChat::command("SAY ".$line);
    HexChat::EAT_ALL;
}

sub ree_handler_privmsg
{
    my $string = $_[1][0];
    my ($mmask,$mnick,$mchan,$mmesg,$first,@msg_array);
    unless (-e $ree_text_dir."/triggers/messages.txt") {
        mkdir $ree_text_dir."/triggers";
        ree_get_missing_file("triggers/messages.txt");
    }
    if( $string =~ /^:(.*?)!(.*?)@(.*?) PRIVMSG (.*?) :(.*?)$/i )
    {
        $mmask = $1."!".$2."@".$3;
        $mnick = $1;
        $mchan = $4;
        $mmesg = $5;
        @msg_array = split(" ", $mmesg);
        $first = $msg_array[0];
    } else { return HexChat::EAT_NONE; }
    open my $handle, '<', $ree_text_dir."/triggers/messages.txt";
    chomp(my @lines = <$handle>);
    foreach $line (@lines)
    {
        chomp($line);
        if($line =~ /^(.*?) (.*?) :(.*?)$/i)
        {
            my $tmatch    = $1;
            my $tchan     = $2;
            my $tcmd      = $3;
            my $pattern   = "^".$tmatch."(\s|$)";
            if(match_glob($tchan, $mchan) && match_glob($tmatch, $first))
            {
                $tcmd =~ s/\%c/$mchan/g;
                $tcmd =~ s/\%n/$mnick/g;
                while($tcmd =~ /\%[0-9]+/)
                {
                    $tcmd =~ s/\%([0-9]+)/$msg_array[$1]/eeg
                }
                HexChat::command($tcmd);
                return HexChat::EAT_NONE;
            }
        }
    }
    close $handle;
    return HexChat::EAT_NONE;
}

sub ree_handler_onPart
{
    my $jstring = $_[1][0];
    my ($jmask,$jnick,$jchan,$preason);
    unless (-e $ree_text_dir."/triggers/parts.txt") {
        mkdir $ree_text_dir."/triggers";
        ree_get_missing_file("triggers/parts.txt");
    }
    if( $jstring =~ /^:(.*?)!(.*?)@(.*?) PART (.*?) :(.*?)$/i )
    {
        $jmask = $1."!".$2."@".$3;
        $jnick = $1;
        $jchan = $4;
        $preason = $5;
    } elsif( $jstring =~ /^:(.*?)!(.*?)@(.*?) PART (.*?)$/i )
    {
        $jmask = $1."!".$2."@".$3;
        $jnick = $1;
        $jchan = $4;
        $preason = "unknown";
    } else { return HexChat::EAT_NONE; }
    open my $handle, '<', $ree_text_dir."/triggers/parts.txt";
    chomp(my @lines = <$handle>);
    foreach $line (@lines)
    {
        chomp($line);
        if($line =~ /^(.*?) (.*?) :(.*?)$/i)
        {
            my $cmask = $1;
            my $cchan = $2;
            my $ccmd  = $3;
            if(match_glob($cmask, $jmask) && match_glob($cchan, $jchan))
            {
                $ccmd =~ s/\%c/$jchan/g;
                $ccmd =~ s/\%n/$jnick/g;
                $ccmd =~ s/\%r/$preason/g;
                HexChat::command($ccmd);
                return HexChat::EAT_NONE;
            }
        }
    }
    close $handle;
    return HexChat::EAT_NONE;
}

sub ree_handler_onJoin
{
    my $jstring = $_[1][0];
    my ($jmask,$jnick,$jchan);
    #ree_msg($jstring);
    unless (-e $ree_text_dir."/triggers/joins.txt") {
        mkdir $ree_text_dir."/triggers";
        ree_get_missing_file("triggers/joins.txt");
    }
    if( $jstring =~ /^:(.*?)!(.*?)@(.*?) JOIN (.*?)(\s|$)/i )
    {
        $jmask = $1."!".$2."@".$3;
        $jnick = $1;
        $jchan = $4;
    } else { return HexChat::EAT_NONE; }
    open my $handle, '<', $ree_text_dir."/triggers/joins.txt";
    chomp(my @lines = <$handle>);
    foreach $line (@lines)
    {
        chomp($line);
        if($line =~ /^(.*?) (.*?) :(.*?)$/i)
        {
            my $cmask = $1;
            my $cchan = $2;
            my $ccmd  = $3;
            if(match_glob($cmask, $jmask) && match_glob($cchan, $jchan))
            {
                $ccmd =~ s/\%c/$jchan/g;
                $ccmd =~ s/\%n/$jnick/g;
                HexChat::command($ccmd);
                return HexChat::EAT_NONE;
            }
        }
    }
    close $handle;
    return HexChat::EAT_NONE;
}

sub ree_fw_add
{
    my ($mask) = @_;
    $mask = lc($mask);
    $ree_pmfw_mask_list{$mask} = 1;
}

sub ree_fw_del
{
    my ($mask) = @_;
    $mask = lc($mask);
    return delete $ree_pmfw_mask_list{$mask};
}

sub ree_pm_firewall_cmd
{
    my $subcmd = $_[0][1];
    if(!$subcmd)
    {
        HexChat::print([
            "\002PM Firewall Help:\002\n",
            "\002ALLOW\002\t\t- Allow a nick+hostmask through the PM firewall\n",
            "\t\t\tUsage: /ree-pmfw ALLOW *!MrRee@*.6697.co.uk\n",
            "\002DELETE\002\t\t- Delete given PM Firewall rule. \002Usage:\002 /ree-pmfw DELETE *!MrRee@*.6697.co.uk\n",
            "\t\t\tUsage: /ree-pmfw DELETE 0\n",
            "If you need more help please join \00318#ree\003 on \00318irc.6697.co.uk\003"
        ]);
    return HexChat::EAT_ALL;
    } elsif($subcmd =~ /^allow$/i)
    {
        ree_fw_add($_[0][2]);
        ree_msg("\00320\002[FIREWALL]\002\003\t Added ".$_[0][2]." to the \00319ALLOW\003 list");
        return HexChat::EAT_ALL;
    } elsif($subcmd =~ /^delete$/i)
    {
        ree_fw_del($_[0][2]);
        ree_msg("\00320\002[FIREWALL]\002\003\t Deleted ".$_[0][2]." from the \00319ALLOW\003 list");
        return HexChat::EAT_ALL;
    } elsif($subcmd =~ /^list$/i)
    {
        foreach $mask (keys %ree_pmfw_mask_list)
        {
            chomp($mask);
            ree_msg("\00320\002[FIREWALL]\003-\00319[ALLOW]\002\003\t".$mask);
        }
        return HexChat::EAT_ALL;
    } elsif($subcmd =~ /^save$/i)
    {
        ree_pmfw_save_rules();
        return HexChat::EAT_ALL;
    } elsif($subcmd =~ /^load$/i)
    {
        ree_pmfw_load_rules();
        return HexChat::EAT_ALL;
    }
}

sub ree_pmfw_load_rules
{
    %ree_pmfw_mask_list = {};
    $ree_pmfw_mask_list{"\%*\!\*@\*"} = 1;
    open my $handle, '<', HexChat::get_info('configdir')."/ree.pmfw";
    chomp(my @lines = <$handle>);
    $i=0;
    foreach $line (@lines)
    {
        $ree_pmfw_mask_list{$line}=1;
        $i++;
    }
    close $handle;
    ree_msg("\00320\002[FIREWALL]\002\003\t ".$i." firewall rules loaded!");
}

sub ree_ascii
{
    $file = $_[0][1];
    if($file =~ /\.\./i)
    {
        ree_msg("[WARNING!]\tIt looks like you tried to traverse directories backwards. This has been blocked.");
        return HexChat::EAT_ALL;
    }
    if($file =~ /^[a-zA-Z0-9\_\-\/]+$/i)
    {
        # Looks good to me!
    } else {
        ree_msg("[WARNING!]\tWoops that didn't look right! Please check and try again!");
        return HexChat::EAT_ALL;
    }
    open my $handle, '<', HexChat::get_info('configdir')."/addons/ree/ascii/".$file.".txt";
    chomp(my @lines = <$handle>);
    foreach $line (@lines)
    {
        HexChat::command("say ".$line);
    }
    close $handle;
    return HexChat::EAT_ALL;
}

sub ree_pmfw_save_rules
{
    open(my $fh, '>', HexChat::get_info('configdir')."/ree.pmfw") or return 0;
    foreach $mask (keys %ree_pmfw_mask_list)
    {
        print $fh $mask."\n";
    }
    close $fh;
    ree_msg("\00320\002[FIREWALL]\002\003\t Firewall rules saved!");
    return 1;
}

sub ree_handler_pmfw
{
    if($_[1][0] =~ /^\:(.*?)\!(.*?)\@(.*?) PRIVMSG (.*?) \:(.*?)$/i)
    {
        my $theMask = $1."!".$2."@".$3;
        $theMask = lc($theMask);
        my $target = $4;
        my $found = 0;
        if(HexChat::plugin_pref_get('ree.pmfw.enabled') =~ /^on$/i)
        {
            foreach $mask (keys %ree_pmfw_mask_list)
            {
                if(match_glob($mask, $theMask))
                {
                    $found = $ree_pmfw_mask_list{$mask};
                }
            }
            if(!$found && match_glob($target, HexChat::get_info('nick')))
            {
                if(HexChat::plugin_pref_get('ree.debug') =~ /^on$/i)
                {
                    ree_msg_window("debug", $theMask." tried to PM you (".$target.") but your PM Firewall is turned on!");
                }
                HexChat::EAT_ALL;
            }
        }
    }
}

sub ree_handler_ctcp
{
    if($_[1][0] =~ /^\:(.*?)\!(.*?)\@(.*?) PRIVMSG (.*?) \:\001VERSION\001$/i)
    {
        my $theMask = $1."!".$2."@".$3;
        my $theNick = $1;
        if(HexChat::plugin_pref_get('ree.handlers.ctcp_random_version') =~ /^on$/i)
        {
            unless (-e $ree_text_dir."/versions.txt") {
                ree_get_missing_file("versions.txt");
            }
            my $rand_version = ree_random_line_from_file($ree_text_dir."/versions.txt");
            if(!$rand_version)
            {
                ree_msg("Could not determine a random version string! Failed to reply to version request from ".$1);
                return HexChat::EAT_ALL;
            }
            ree_msg("\00318".$theMask."\003 has requested \002VERSION\002 from you! Sending: ".$rand_version);
            HexChat::command("QUOTE PRIVMSG ".$theNick." :\001VERSION ".$rand_version."\001");
            return HexChat::EAT_ALL;
        }
    }
}

sub ree_slap
{
    if(!$_[0][1]) { $_[0][1] = 'nobody'; }
    unless (-e $ree_text_dir."/slaps.txt") {
        ree_get_missing_file("slaps.txt");
    }
    my $line = ree_random_line_from_file($ree_text_dir."/slaps.txt");
    if($line)
    {
        HexChat::command("ME slaps ".$_[0][1]." with ".$line);
    } else {
        ree_msg("\00304Error!:\003\tCould not open slaps file!");
    }
    HexChat::EAT_ALL;
}

sub ree_give
{
    if(!$_[0][1]) { $_[0][1] = 'nobody'; }
    unless (-e $ree_text_dir."/gives.txt") {
        ree_get_missing_file("gives.txt");
    }
    my $line = ree_random_line_from_file($ree_text_dir."/gives.txt");
    if($line)
    {
        HexChat::command("ME gives ".$_[0][1]." ".$line);
    } else {
        ree_msg("\00304Error!:\003\tCould not open gives file!");
    }
    HexChat::EAT_ALL;
}

sub ree_print_from_file
{
    my $line = ree_random_line_from_file($_[0][1]);
    if($line)
    {
        ree_msg($line);
    } else {
        ree_msg("\00304Error!:\003\tCould not open file: ".$_[0][1]);
    }
    HexChat::EAT_ALL;
}

sub ree_random_line_from_file
{
    my ($filename) = @_;
    srand;
    open FILE, $filename or return undef;
    rand($.)<1 and ($line=$_) while <FILE>;
    close FILE;
    chomp($line);
    return $line;
}

## AUTO AWAY STUFF ##
sub ree_set_idle_time {
	ree_msg("Idle time set to ".$_[0][1]." seconds.");
	HexChat::plugin_pref_get('ree.autoaway.timer', $_[0][1]*1000);
	return HexChat::EAT_HEXCHAT;
}

sub ree_timer_check {
	my $cServ = get_info("server");
	my $pos = ree_pos_in_list(\@servers,$cServ);
	if (defined $timers[$pos]) {
		unhook($timers[$pos]);
	}
	if (defined get_info("away")) {
		ree_set_back($cServ);
	}
	$timers[$pos] = hook_timer(HexChat::plugin_pref_get('ree.autoaway.timer'), \&ree_set_away, $cServ);
	return HexChat::EAT_NONE;
};

sub ree_add_new_server {
	ree_populate_servers();
	return HexChat::EAT_NONE;
}

sub ree_populate_servers {
	my @info = get_list("channels");
	foreach my $chan (@info) {
		my $cServ = $chan->{server};
		if (ree_pos_in_list(\@servers,$cServ)==-1) {
			push(@servers,$cServ);
			my $timer = hook_timer(HexChat::plugin_pref_get('ree.autoaway.timer'), \&ree_set_away, $cServ);
			push(@timers,$timer);
		}
	}
};

#returns the position in an array of a given element, or -1 if it is not found
#arguments: the array to look in, the element to look for
sub ree_pos_in_list {
	my $pos = -1;
	my @servers = @{$_[0]};
		for (my $cPos = 0; $cPos < scalar(@servers); $cPos++) {
			if ($servers[$cPos] eq $_[1]) {
				$pos = $cPos;
			}
		}
	return $pos;
}

sub ree_set_away {
	my $context = find_context(undef,$_[0]);
	if (!defined context_info($context)->{away}) {
		command("away",undef,$_[0]);
	}
	return REMOVE;
};

sub ree_set_back {
	command("back",undef,$_[0]);
}

sub ree_get_missing_file {
    my ($missing_file) = @_;
    chomp($missing_file);
    my $ree_update_content = get 'https://gitlab.com/api/v4/projects/13179260/releases';
    if(!$ree_update_content) 
    { 
        ree_msg("Warning! Could not get latest version information for ree.pl.");
    } else {
        if ( $ree_update_content =~ /\"tag_name\"\:\"([0-9\.]+).*?\"/i )
        {
            my $ree_latest_version = $1;
            ree_msg("Attempting to download missing file: ".$missing_file);
            my $ree_update_file = get 'https://gitlab.com/MrRee/ree.pl/raw/'.$ree_latest_version.'/ree/'.$missing_file;
            if(length($ree_update_file) == 0) 
            {
                ree_msg($missing_file." is empty!");
                return;
            }
            if (-e $ree_text_dir and -d $ree_text_dir) {
                # directory exists, continue.
            } else {
                mkdir $ree_text_dir or return HexChat::EAT_ALL;
            }
            open(my $fh, '>', $ree_text_dir."/".$missing_file) or return 0;
            print $fh $ree_update_file;
            close $fh;
            ree_msg("Successfully downloaded ".$missing_file." from latest stable release!");
        }
    }
    HexChat::EAT_ALL;   
}

ree_init();