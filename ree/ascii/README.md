# ASCII folder
Any file that you put in this folder (or subfolders) that ends with .txt can be called with the `/ree-ascii` command followed by the path relative to this folder.  
If your file is at `ascii/sample/mirc.txt` (for example) then you might call it like so: `/ree-ascii sample/mirc`.

# ASCII Sources
* [github.com/ircart/ircart](https://github.com/ircart/ircart): This is an unfiltered list of IRC art. Some of it may be considered distasteful.