# Introduction
This script is meant for Linux users of HexChat, and will provide a bunch of new features!

# Requirements
Before you can use this script you will need to install the following modules with `cpan`:
* `LWP::Simple`
* `JSON`
* `Text::Glob`

# What it does so far
* OS detection, it shouldn't work on Windows but in case someone gets it working then it will bind Windows commands instead of Linux commands.
* Update checker
* Rhythmbox now playing (`/ree-np`)
* Put incoming server messages into separate windows for easy reading.
* Blocks unwanted private messages, allowing people to PM you based on a filter.
* Rainbow text (`/ree-rainbow`)
* IP Info (`/ree-ipinfo`)
* Auto Away/Back system.
* Random slaps (`/ree-slap <nickname>`).
* Random gives (`/ree-give <nickname>`).
* ASCII art (`/ree-ascii <path>`).
* Automatically downloads missing files.
* Random Cards against humanity question+answer combo. (`/ree-cah`).
* Triggers:
  * On-Join triggers
  * On-Part triggers

# Triggers
**On-Message Example:** `!cah #channel :REE-CAH`  
**On-Join Example:** `*!*@* #channel :SAY Welcome to %c %n!`  
**On-Part Example:** `*!*@* #channel :SAY Awe, why did %n have to go so soon?`
## Macros
### On-Join
* `%c` - The channel that was joined.
* `%n` - The nickname that joined.

### On-Part
* `%c` - The channel that was joined.
* `%n` - The nickname that joined.
* `%r` - The part reason.

### On-Message
New macros!!
* `%[0-9]+` - `%` followed by a number will be replaced with the given argument. Starts at `%0` for the command, and `%1` for the first argument.

# PM Firewall
To enable or disable the firewall, you need to use the `/ree-set` command: `/ree-set pmfw.enabled [on|off]`. The firewall will need to be enabled before it's rules will go into effect.
The default (hidden) rule is to block everything, you will need to explicitly allow messages to come through by hostmask.

* To allow a host: `/ree-pmfw ALLOW *!MrRee@6697.co.uk`
* To delete a rule: `/ree-pmfw DELETE *!MrRee@6697.co.uk`
* To list rules: `/ree-pmfw LIST`
* Save to disk: `/ree-pmfw SAVE`
* Load from disk: `/ree-pmfw LOAD`
* For more help: `/ree-pmfw`

# TODO
* ~~Update script via command (latest stable or development)~~
* ~~Automatically update missing files from latest stable branch.~~
* ~~Configuration files~~
* Other player options for now playing
* Fun stuff:
  * ~~fish slaps~~
  * ~~gives~~
  * other random actions.
* Useful stuff:
  * Auto-reply when away
  * Regain nick
* Security & stealth features:
  * ~~Randomized versions~~
  * Randomized quit messages
  * Randomized part messages
  * PM "Firewall"
    * ~~Block private messages that aren't from users on ALLOW list.~~
    * Block private messages that match a filter.
    * Let user know when they have a blocked PM via a setting other than `debug`. (For now just use `/ree-set debug on`).
      * Furthermore when a user does have a blocked pm offer to put the mask on the ALLOW list or give the proper command to do so.
* Triggers:
  * ~~On-Join~~
  * ~~On-Part~~
  * ~~On-Message~~

# Changelog
## 0.06
* More Triggers
  * Now triggers on incoming messages

## 0.05
* `/ree-cah` - Generates and says a random Cards Against Humanity question with filled answers.
* Triggers
  * Perform commands on join.
  * Perform commands on part.
 
## 0.04
* Automatically download missing files.
## 0.03
* Auto-updater (`/ree-install-update`)
* Option for checking for updates on startup (`/ree-set update.startup.check [on|off]` default on).
* Option for installing updates on startup (`/ree-set update.startup.run [on|off]` default: off)
## 0.02
* Initial commit, all currently finished features are in this release.